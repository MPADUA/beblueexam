# README #

Projeto para participação do processo seletivo beblue.

Neste projeto consta as classes com seus métodos, atributos e regras de negócio de um cashback.

Pelo motivo de sair de casa para trabalhar às 05:30 e voltar às 19:30 acredito não ter tempo suficiente para finalizar o objetivo do exame, porém fiz o máximo que pude, ficou pendente a disponibilização do Rest com Spring, assim não conseguindo executar os teste com o Json.

No projeto consta a página index.jsp que foi utilizada para executar os testes da rotina de cashback.

Agradeço a oportunidade.

Maurício Urbinati de Pádua.