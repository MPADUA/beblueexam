package DAO;

import Classes.*;
import Hibernate.*;
import java.util.List;
import org.hibernate.*;
import org.hibernate.criterion.Restrictions;

public class Transacao_UsuarioDAO {
    
    private SessionFactory sf;
    private Session session;
    
    public Transacao_UsuarioDAO(){
        this.sf = HibernateUtil.getSessionFactory();
        this.session = sf.openSession(); 
    }
    
    public void closeSession(){
        this.sf.close();
    }
    
    public String save(Transacao_Usuario transacaoUsuario){
        String msgAlert = "";
        try{
            Transaction transaction = this.session.beginTransaction();
            this.session.save(transacaoUsuario);
            transaction.commit();
            msgAlert = "Salvo com Sucesso!";
        }catch(Exception e){
            msgAlert = e.getMessage();
        }
        this.session.close();
        return msgAlert;
    }
    
    public String delete(Transacao_Usuario transacaoUsuario){
        String msgAlert = "";
        try {
            Transaction transaction = this.session.beginTransaction();
            this.session.delete(transacaoUsuario);
            transaction.commit();
            msgAlert = "Deletado com Sucesso!";
        } catch (Exception e) {
            msgAlert = e.getMessage();
        }
        this.session.close();
        return msgAlert;
    }
    
    public String update(Transacao_Usuario transacaoUsuario){
        String msgAlert = "";
        try{
            Transaction transaction = this.session.beginTransaction();
            this.session.update(transacaoUsuario);
            transaction.commit();
            msgAlert = "Atualizado com Sucesso!";
        }catch (Exception e){
            msgAlert = e.getMessage();
        }
        this.session.close();
        return msgAlert;
    }
    
    //user-transaction
    public List<Transacao_Usuario> buscaTransacao(Usuario cpf){
        List<Transacao_Usuario> tus = null;
        try{
            Transaction transaction = this.session.beginTransaction();
            Criteria cri = session.createCriteria(Transacao_Usuario.class);
            cri.add(Restrictions.eq("usuario", cpf));
            tus = cri.list();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        this.session.close();
        return tus;
    }
    
    //users-transaction
    public List<Transacao_Usuario> buscaTransacao(){
        List<Transacao_Usuario> tus = null;
        try{
            Transaction transaction = this.session.beginTransaction();
            Criteria cri = session.createCriteria(Transacao_Usuario.class);
            tus = cri.list();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        this.session.close();
        return tus;
    }
}
