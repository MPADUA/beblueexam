package DAO;

import Classes.*;
import Hibernate.*;
import java.util.List;
import org.hibernate.*;
import org.hibernate.criterion.Restrictions;

public class UsuarioDAO {
    
    private SessionFactory sf;
    private Session session;
    
    public UsuarioDAO(){
        this.sf = HibernateUtil.getSessionFactory();
        this.session = sf.openSession(); 
    }
    
    public void closeSession(){
        this.sf.close();
    }
    
    public String save(Usuario usuario){
        String msgAlert = "";
        try{
            Transaction transaction = this.session.beginTransaction();
            this.session.save(usuario);
            transaction.commit();
            msgAlert = "Salvo com Sucesso!";
        }catch(Exception e){
            msgAlert = e.getMessage();
        }
        this.session.close();
        return msgAlert;
    }
    
    public String delete(Usuario usuario){
        String msgAlert = "";
        try {
            Transaction transaction = this.session.beginTransaction();
            this.session.delete(usuario);
            transaction.commit();
            msgAlert = "Deletado com Sucesso!";
        } catch (Exception e) {
            msgAlert = e.getMessage();
        }
        this.session.close();
        return msgAlert;
    }
    
    public String update(Usuario usuario){
        String msgAlert = "";
        try{
            Transaction transaction = this.session.beginTransaction();
            this.session.update(usuario);
            transaction.commit();
            msgAlert = "Atualizado com Sucesso!";
        }catch (Exception e){
            msgAlert = e.getMessage();
        }
        this.session.close();
        return msgAlert;
    }
    
    public List<Usuario> busca(String cpf){
        List<Usuario> users = null;
        try{
            Transaction transaction = this.session.beginTransaction();
            Criteria cri = session.createCriteria(Usuario.class);
            cri.add(Restrictions.eq("cpf", cpf));
            users = cri.list();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        this.session.close();
        return users;
    }
    
    //users
    public List<Usuario> busca(){
        List<Usuario> users = null;
        Query qry = null;
        try{
            Transaction transaction = this.session.beginTransaction();
            Criteria cri = session.createCriteria(Usuario.class);
            users = cri.list();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        this.session.close();
        return users;
    }
    
}
