package DAO;

import Classes.*;
import Hibernate.*;
import java.util.List;
import org.hibernate.*;
import org.hibernate.criterion.Restrictions;

public class TransacaoDAO {
    
    private SessionFactory sf;
    private Session session;
    
    public TransacaoDAO(){
        this.sf = HibernateUtil.getSessionFactory();
        this.session = sf.openSession(); 
    }
    
    public void closeSession(){
        this.sf.close();
    }
    
    public String save(Transacao transacao){
        String msgAlert = "";
        try{
            Transaction transaction = this.session.beginTransaction();
            this.session.save(transacao);
            transaction.commit();
            msgAlert = "Salvo com Sucesso!";
        }catch(Exception e){
            msgAlert = e.getMessage();
        }
        this.session.close();
        return msgAlert;
    }
    
    public String delete(Transacao transacao){
        String msgAlert = "";
        try {
            Transaction transaction = this.session.beginTransaction();
            this.session.delete(transacao);
            transaction.commit();
            msgAlert = "Deletado com Sucesso!";
        } catch (Exception e) {
            msgAlert = e.getMessage();
        }
        this.session.close();
        return msgAlert;
    }
    
    public String update(Transacao transacao){
        String msgAlert = "";
        try{
            Transaction transaction = this.session.beginTransaction();
            this.session.update(transacao);
            transaction.commit();
            msgAlert = "Atualizado com Sucesso!";
        }catch (Exception e){
            msgAlert = e.getMessage();
        }
        this.session.close();
        return msgAlert;
    }
    
    public List<Transacao> busca(String transacao){
        List<Transacao> trans = null;
        try{
            Transaction transaction = this.session.beginTransaction();
            Criteria cri = session.createCriteria(Transacao.class);
            cri.add(Restrictions.eq("tipo", transacao));
            trans = cri.list();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        this.session.close();
        return trans;
    }
}
