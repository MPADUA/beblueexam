package WebService;

import DAO.*;
import Classes.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

//@RestController
public class Service {
    
    @RequestMapping(method= RequestMethod.GET, value="/user-name/{cpf}")
    @ResponseBody
    public Usuario buscaUser(@PathVariable("cpf") String cpf){
        UsuarioDAO uDAO = new UsuarioDAO();
        return uDAO.busca(cpf).get(0);
    }
    
}
