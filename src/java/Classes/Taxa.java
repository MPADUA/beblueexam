package Classes;

//Classe para parametrizar porcentagens por dia, ideal fazer parametrização no BD.

import java.util.Calendar;
import org.eclipse.jdt.internal.compiler.ast.ThisReference;

public class Taxa {

    static Double TX_DOMINGO    = 0.05;
    static Double TX_SEGUNDA    = 0.10;
    static Double TX_TERCA      = 0.15;
    static Double TX_QUARTA     = 0.20;
    static Double TX_QUINTA     = 0.25;
    static Double TX_SEXTA      = 0.30;
    static Double TX_SABADO     = 0.35;
    
    static Double getTaxa(Calendar data){
        Integer semana = data.get(data.DAY_OF_WEEK); //pegar dia da semana na data passada no parâmetro
        Double taxa = null;
        
        if (semana == 1) {
            taxa = TX_DOMINGO;
        }else if (semana == 2) {
            taxa = TX_SEGUNDA;
        }else if (semana == 3) {
            taxa = TX_TERCA;
        }else if (semana == 4) {
            taxa = TX_QUARTA;
        }else if (semana == 5) {
            taxa = TX_QUINTA;
        }else if (semana == 6) {
            taxa = TX_SEXTA;
        }else if (semana == 7) {
            taxa = TX_SABADO;
        }
        
        return taxa;
    }
    
    
}
