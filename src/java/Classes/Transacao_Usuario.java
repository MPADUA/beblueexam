package Classes;

import DAO.*;
import java.text.DecimalFormat;
import java.util.Calendar;
import javax.persistence.Entity;
import javax.persistence.*;

@Entity
public class Transacao_Usuario {
    
    @Id
    //@GeneratedValue(strategy= GenerationType.AUTO)
    private String     codigo;
    @Temporal(TemporalType.TIMESTAMP)
    @Column
    private Calendar    data;
    @Column
    private double      valor;    
    @ManyToOne
    @JoinColumn(name="usuario_id")
    private Usuario     usuario;
    @ManyToOne
    @JoinColumn(name="transacao_id")
    private Transacao   transacao;
    @Column
    private Integer     merchant;
    
    //Getters and Setters
    public String getCodigo() {
        return codigo;
    }
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Calendar getData() {
        return data;
    }
    public void setData(Calendar data) {
        this.data = data;
    }

    public Transacao getTransacao() {
        return transacao;
    }
    public void setTransacao(Transacao transacao) {
        this.transacao = transacao;
    }

    public Usuario getUsuario() {
        return usuario;
    }
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public double getValor() {
        return valor;
    }
    public void setValor(double valor) {
        this.valor = valor;
    }
    
    // Métodos Construtores
    public Transacao_Usuario() {
        Transacao_UsuarioDAO tuDAO = new Transacao_UsuarioDAO();
        tuDAO.closeSession();
    }
    public Transacao_Usuario(String cpf, Integer merchant, double valor, String transacao) {
        Transacao_UsuarioDAO tuDAO = new Transacao_UsuarioDAO();
        tuDAO.closeSession();
        
        UsuarioDAO uDAO = new UsuarioDAO();
        TransacaoDAO tDAO = new TransacaoDAO();
        
        this.merchant   = merchant;
        this.valor      = valor;
        this.usuario    = uDAO.busca(cpf).get(0);
        this.transacao  = tDAO.busca(transacao).get(0);;
        this.data       = Calendar.getInstance();
        this.codigo     =  this.usuario.getCpf().substring(7)+ this.data.get(this.data.DAY_OF_MONTH)  + this.data.get(this.data.SECOND)+ this.data.get(this.data.HOUR_OF_DAY);
        this.codigo     += this.data.get(this.data.MONTH)  + this.data.get(this.data.MINUTE) + this.data.get(this.data.YEAR);
    }
    
    // Métodos
    //register-transaction
    public String save(){
        
        Double newSaldo = 0.0;
        String ret      = "";
        
        Transacao_UsuarioDAO tuDAO = new Transacao_UsuarioDAO();
        
        newSaldo = calculaSaldo(this.getData(), this.getValor());
        if (this.transacao.getTipo().toString().equals("TP_1")) {
            // VERIFICA SE EXISTE SALDO SUFICIENTE
            this.getUsuario().setSaldo(this.usuario.getSaldo() - this.getValor());
            if (this.getUsuario().getSaldo() > 0){
                this.getUsuario().update();
                tuDAO.save(this);
                ret = "Saldo atualizado! Você utilizou R$ " + String.format("%.2f", this.getValor());
            }else{
                ret = "Saldo insulficiente para realizar a compra";
            }
                
        }else{
            this.getUsuario().setSaldo(this.usuario.getSaldo() + newSaldo);
            this.getUsuario().update();
            tuDAO.save(this);
            ret = "Saldo atualizado! Você recebeu R$ " + String.format("%.2f", newSaldo);
        }
    
        return ret;
    }
    
    private Double calculaSaldo(Calendar data, double valor){
        return valor * Taxa.getTaxa(data);
    }    
}
