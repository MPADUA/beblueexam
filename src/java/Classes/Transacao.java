package Classes;

import DAO.*;
import java.util.*;
import javax.persistence.*;

@Entity
public class Transacao {
    @Id
    private String  tipo;
    @Column(nullable=false)
    private String  nome;
    @OneToMany(cascade= CascadeType.ALL, mappedBy="transacao")
    private List<Transacao_Usuario> listaTransacao ;
    
    //Getters and Setters
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTipo() {
        return tipo;
    }
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    // Métodos Construtores
    public Transacao() {
        TransacaoDAO tDAO = new TransacaoDAO();
        tDAO.closeSession();
    }
    public Transacao(String tipo, String nome) {
        TransacaoDAO tDAO = new TransacaoDAO();
        tDAO.closeSession();
        
        this.tipo    = tipo;
        this.nome    = nome;
    }
    
    // Métodos
    public String save(){
        TransacaoDAO transacaoDAO = new TransacaoDAO();
        return transacaoDAO.save(this);
    }
    public String delete(){
        TransacaoDAO transacaoDAO = new TransacaoDAO();
        return transacaoDAO.delete(this);
    }
    public String update(){
        TransacaoDAO transacaoDAO = new TransacaoDAO();
        return transacaoDAO.update(this);
    }
}
