package Classes;

import DAO.*;
import java.text.DecimalFormat;
import java.util.List;
import javax.persistence.*;
import javax.xml.bind.annotation.*;

@Entity
public class Usuario {
    
    @Id
    private String cpf;
    @Column(nullable=false)
    private String  nome;
    @Column(nullable=false)
    private Double  saldo;
    @OneToMany(cascade= CascadeType.ALL, mappedBy="usuario")
    private List<Transacao_Usuario> listaTransacao;

    // Getters and Setters
    public String getCpf() {
        return cpf;
    }
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getSaldo() {
        return saldo;
    }
    public void setSaldo(Double saldo) {
        this.saldo = Double.valueOf(String.format("%.2f", saldo).replace(",", "."));
        //this.saldo = saldo;
    }

    // Métodos Construtores
    public Usuario() {
        UsuarioDAO uDAO = new UsuarioDAO();
        uDAO.closeSession();
    }
    public Usuario(String cpf, String nome, Double saldo) {
        UsuarioDAO uDAO = new UsuarioDAO();
        uDAO.closeSession();
        
        this.cpf    = cpf;
        this.nome   = nome;
        this.saldo  = saldo;
    }
    
    // Métodos
    public String save(){
        UsuarioDAO usuarioDAO = new UsuarioDAO();
        return usuarioDAO.save(this);
    }
    public String delete(){
        UsuarioDAO usuarioDAO = new UsuarioDAO();
        return usuarioDAO.delete(this);
    }
    public String update(){
        UsuarioDAO usuarioDAO = new UsuarioDAO();
        return usuarioDAO.update(this);
    }
}
